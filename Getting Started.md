# Getting started with Jupyter in BizOps

Getting started with Jupyter in BizOps is easy, with just a few things to keep in mind.

## Create a credentials file

You will likely want to share your notebook, or perhaps perform something like screen share. To facilitate this, we want to ensure there are no credentials stored in the notebook itself. 

Instead, we recommend creating a separate file for credentials and other private settings. 

1. In Jupyter, create a new directory in the file explorer pane on the left. There is an icon at the top of a folder with a `+` on it.
1. Name the folder `settings`, by right clicking on it and selecting `Rename`.
1. Right click your new folder, and click Open.
1. Create a new file in this folder, by clicking the `+` icon at the top. Pick `Text Editor` under the `Other` category.
1. Enter your database credentials here, for example:

	```json
	{
		"username": "joshua",
		"password": "123456",
		"db_host": "dev-bizops",
		"db_name": "dw_production"
	}
	```

1. Save the file, using the File menu or `command-S` on Mac.
1. Right click the filename in the tab, and select `Rename`. Rename the file to `settings.json`.
1. Note going forward to edit this file you may have to right click the file, choose `Open` and then select `Editor`. The `JSON` viewer does not seem to load presently.
1. In the file explorer, click on the Home icon to return to your root folder.

## Creating your first notebook

You are now ready to create your first notebook. 

1. Click on the `+` icon in the file explorer, and choose `Python 3`.
1. Import a few dependencies that we will be working with:

	```python
	import sqlalchemy
	import psycopg2
	import json
	import matplotlib.pyplot as plt
	%load_ext sql
	```

1. Next, we will create the database connection string and connect:

	```python
	config = json.load(open("settings/settings.json"))
	db_connection = 'postgresql+psycopg2://' + config['username'] + ':' + config['password'] + '@' + config['db_host'] + '/' + config['db_name'];
	%sql $db_connection
	```

1. If you got a connection error, ensure the credentials in your `settings/settings.json` file are correct.
1. With a succesfull database connection, we can start to run queries. For example here is one from our Feature Adoption dashboard, looking at the number of users.

	```python
	%%sql

	SELECT 
		TO_CHAR(DATE_TRUNC('month', (usage_data.created_at )::timestamptz AT TIME ZONE 'America/Los_Angeles'), 'YYYY-MM') AS "usage_data.created_at_month",
		AVG(usage_data.active_user_count ) AS "usage_data.average_users",
		(SELECT AVG(a) FROM UNNEST((ARRAY(SELECT UNNEST(ARRAY_AGG(usage_data.active_user_count )) ORDER BY 1))[(SELECT CAST(FLOOR(COUNT(usage_data.active_user_count ) * 0.8 - 0.0000001) AS INTEGER) + 1):(SELECT CAST(FLOOR(COUNT(usage_data.active_user_count ) * 0.8) AS INTEGER) + 1)]) a) AS "usage_data.percentile80_users",
		(SELECT AVG(a) FROM UNNEST((ARRAY(SELECT UNNEST(ARRAY_AGG(usage_data.active_user_count )) ORDER BY 1))[(SELECT CAST(FLOOR(COUNT(usage_data.active_user_count ) * 0.9 - 0.0000001) AS INTEGER) + 1):(SELECT CAST(FLOOR(COUNT(usage_data.active_user_count ) * 0.9) AS INTEGER) + 1)]) a) AS "usage_data.percentile90_users",
		(SELECT AVG(a) FROM UNNEST((ARRAY(SELECT UNNEST(ARRAY_AGG(usage_data.active_user_count )) ORDER BY 1))[(SELECT CAST(FLOOR(COUNT(usage_data.active_user_count ) * 0.99 - 0.0000001) AS INTEGER) + 1):(SELECT CAST(FLOOR(COUNT(usage_data.active_user_count ) * 0.99) AS INTEGER) + 1)]) a) AS "usage_data.percentile99_users"
	FROM version.usage_data  AS usage_data

	WHERE 
		(((usage_data.created_at ) >= ((SELECT ((DATE_TRUNC('month', DATE_TRUNC('day', CURRENT_TIMESTAMP AT TIME ZONE 'America/Los_Angeles')) + (-11 || ' month')::INTERVAL) AT TIME ZONE 'America/Los_Angeles'))) AND (usage_data.created_at ) < ((SELECT (((DATE_TRUNC('month', DATE_TRUNC('day', CURRENT_TIMESTAMP AT TIME ZONE 'America/Los_Angeles')) + (-11 || ' month')::INTERVAL) + (12 || ' month')::INTERVAL) AT TIME ZONE 'America/Los_Angeles')))))
	GROUP BY DATE_TRUNC('month', (usage_data.created_at )::timestamptz AT TIME ZONE 'America/Los_Angeles')
	ORDER BY 1 DESC
	LIMIT 500
	```

1. This will output a table, let's store it for later use.

	```python
	df = _.DataFrame()
	```

1. Next, let's create a line chart based on the results we just retrieved.

	```python
	plt.style.use('seaborn-darkgrid')
	plt.plot('usage_data.created_at_month', 'usage_data.average_users', data=df, color='blue')
	plt.plot('usage_data.created_at_month', 'usage_data.percentile80_users', data=df, color='red')
	plt.plot('usage_data.created_at_month', 'usage_data.percentile90_users', data=df, color='yellow')
	plt.plot('usage_data.created_at_month', 'usage_data.percentile99_users', data=df, color='green')
	plt.legend()
	plt.title('Users per instance')
	```
